package Model;

import Dao.Data;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author internet
 */
public class Cliente {

    private int id;
    private String nome;
    private String email;
    private String telefone;
    private Data dataSource;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String del(int id) {
        try {
            String SQL = "DELETE FROM cliente WHERE id=" + id;
            return "Del com sucesso";
        } catch (Exception ex) {
            System.err.println("Erro");
        }
        return null;
    }

    public ArrayList<Cliente> read(int id) {
        try {
            String SQL = "SELECT * FROM cliente WHERE id=" + id;
            PreparedStatement ps = dataSource.GetConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();

            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while (rs.next()) {
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }
            ps.close();
        } catch (SQLException ex) {
            System.err.println("Erro ao recuperar ");
        } catch (Exception ex) {
            System.err.println("Erro");
        }
        return null;
    }

    public ArrayList<Cliente> readAll() {
        try {
            String SQL = "SELECT * FROM cliente";
            PreparedStatement ps = dataSource.GetConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();

            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while (rs.next()) {
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }
            ps.close();
        } catch (SQLException ex) {
            System.err.println("Erro ao recuperar ");
        } catch (Exception ex) {
            System.err.println("Erro");
        }
        return null;
    }

}
